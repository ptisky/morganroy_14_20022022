import React from "react";
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";

import "./css/theme.css";

import Home from "./components/Home.jsx";
import NotFound from "./components/NotFound.jsx";
import NewEmployee from "./components/newEmployee.jsx";
import ListEmployee from "./components/listEmployee.jsx";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="*" element={<NotFound />} />
                    <Route index element={<Home />} />
                    <Route path="/newEmployee" element={<NewEmployee />} />
                    <Route path="/listEmployee" element={<ListEmployee />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
