import React from "react";
import ReactDOM from "react-dom";
import "./css/index.css";
import App from "./App";

import { Provider } from 'react-redux'
import rootReducers from './components/State/Reducers'
import { createStore } from 'redux'

const store = createStore(
    rootReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
    document.getElementById('root')
);