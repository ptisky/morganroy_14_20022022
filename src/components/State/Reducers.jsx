const initialState = { employees: [] }

/**
 * Reducer to pass variable between pages
 * @param state
 * @param action
 * @returns {{employees: []}|{employees: *[]}}
 */
function rootReducers(state = initialState, action) {
    switch(action.type) {
        case 'NEW_EMPLOYEE':
            return {
                employees: [...state.employees, action.employee ]
            }
        default:
            return state
    }
}

export default rootReducers