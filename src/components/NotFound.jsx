import React from "react";
import { Link } from "react-router-dom";

/**
 * Not found page
 * @returns {JSX.Element}
 * @constructor
 */
const NotFound = () => {
    return (
        <div className="notFound">
            <span>404</span>
            <Link to="/">Go back home</Link>
        </div>
    );
};

export default NotFound;
