import React from "react";
import { Link } from "react-router-dom";

/**
 * Display html homepage
 * @returns {JSX.Element}
 * @constructor
 */
const Home = () => {
    return (
        <section id="homepage">
            <Link className="glassIco" to="newEmployee">New employee</Link>
            <Link className="glassIco" to="listEmployee">View all employees</Link>
        </section>
    );
};

export default Home;
