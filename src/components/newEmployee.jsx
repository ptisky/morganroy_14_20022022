import React, { useState } from 'react';
import { Link } from "react-router-dom";
import Select from 'react-select';
import {optionsState, optionsDepartment} from "./Utils/Const.jsx";
import { useDispatch } from 'react-redux';

import { Modal, ModalBody, ModalFooter, ModalHeader } from 'modal-react-op';

/**
 * Display a form to register an employee
 * @returns {JSX.Element}
 * @constructor
 */
const NewEmployee = () => {

    //Set states
    const dispatch = useDispatch();
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [birthDate, setBirthDate] = useState("");
    const [startDate, setStartDate] = useState("");
    const [street, setStreet] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zipCode, setZipCode] = useState("");
    const [department, setDepartment] = useState("");

    const [showModal, setShowModal] = useState(false);
    const [modal, setModal] = useState("");

    //Onsubmit
    function handleSubmit(e) {
        e.preventDefault();

        //Check if all input has been filled
        if(firstName !== "" &&
            lastName !== "" &&
            street !== "" &&
            city !== "" &&
            state !== "" &&
            zipCode !== "" &&
            birthDate !== "" &&
            startDate !== "" &&
            department !== ""){

            //new employee
            const employee = {
                firstName: firstName,
                lastName: lastName,
                birthDate: birthDate,
                startDate: startDate,
                street: street,
                city: city,
                state: state,
                zipCode: zipCode,
                department: department
            }

            //fill object
            dispatch({ type: 'NEW_EMPLOYEE', employee: employee });

            //Display modal
            setModal(modalText.success);
            setShowModal(true)
        }else{
            //Display modal
            setModal(modalText.error);
            setShowModal(true)
        }
    }

    //Set modal text
    const modalText = {
        success : 'Employee Created!',
        error : 'Thank you fill in all fields',
    }

    return (
        <section id="newEmployee">
            <div className="color"/>
            <div className="color"/>
            <div className="color"/>
            <div className="box">
                <div className="square one"/>
                <div className="square two"/>
                <div className="square three"/>
                <div className="square four"/>
                <div className="square five"/>
                <div className="container">
                    <div className="form">
                        <h2>Login Form</h2>
                        <form onSubmit={(e) => handleSubmit(e)}>
                            <div className="inputBox">
                                <input type="text" placeholder="First Name" onChange={(e) => setFirstName(e.target.value)} />
                            </div>
                            <div className="inputBox">
                                <input type="text" placeholder="Last Name" onChange={(e) => setLastName(e.target.value)}/>
                            </div>
                            <div className="inputBox">
                                <label>Date of Birth</label>
                                <input type="date"  onChange={(e) => setBirthDate(e.target.value)}/>
                            </div>
                            <div className="inputBox">
                                <label>Start Date</label>
                                <input type="date"  onChange={(e) => setStartDate(e.target.value)}/>
                            </div>

                            <fieldset>
                                <legend>Address</legend>
                                <div className="inputBox">
                                    <input type="text" placeholder="Street" onChange={(e) => setStreet(e.target.value)}/>
                                </div>

                                <div className="inputBox">
                                    <input type="text" placeholder="City" onChange={(e) => setCity(e.target.value)}/>
                                </div>

                                <div className="inputBox">
                                    <Select
                                        placeholderText='State'
                                        onChange={(e) => setState(e.label)}
                                        options={optionsState}
                                    />
                                </div>

                                <div className="inputBox">
                                    <input type="number" placeholder="Zip Code" onChange={(e) => setZipCode(e.target.value)}/>
                                </div>
                            </fieldset>

                            <div className="inputBox">
                                <Select
                                    placeholderText='Department'
                                    onChange={(e) => setDepartment(e.label)}
                                    options={optionsDepartment}
                                />
                            </div>

                            <div className="inputBox">
                                <input type="submit" value="Save" />
                            </div>
                            <Link to="/" className="forget">Go back home</Link>
                        </form>
                    </div>
                </div>

                <Modal
                    show={showModal}
                    setShow={setShowModal}
                    // hideCloseButton
                >
                    <ModalHeader>
                        <h2>Employee from</h2>
                    </ModalHeader>
                    <ModalBody>
                        <p style={{ textAlign: 'justify' }}>
                            {modal}
                        </p>
                    </ModalBody>
                    <ModalFooter>
                        <a onClick={() => setShowModal(false)}>
                            OK :)
                        </a>
                    </ModalFooter>
                </Modal>
            </div>

        </section>


    );
};

export default NewEmployee;
