# Wealth Health

## Prerequisites for install
- You need [Git](https://git-scm.com) to clone the repository

## Dependencies

- [React](https://reactjs.org): v17.0.2
- [React-router-dom](https://reactrouter.com/web/guides/quick-start): v6.2.1   
- [react-dom](https://fr.reactjs.org/docs/getting-started.html): v17.0.2  
- [react-globally](https://www.npmjs.com/package/react-globally): v1.1.0   
- [react-redux](https://react-redux.js.org/): v7.2.6  
- [react-scripts](https://www.npmjs.com/package/react-scripts): v5.0.0   
- [react-select](https://react-select.com/home): v5.2.2 
- [material-table](https://material-table.com/#/): v1.69.3
- [modal-react-op](https://gitlab.com/ptisky/modal-react-op): v1.0.0

- Recommended text editor: [Visual Studio Code](https://code.visualstudio.com)    

## Installing and launching Front-End    
1. Clone the repository of Wealth Health front-end:   
`git clone https://gitlab.com/ptisky/MorganROY_13_15122021.git`    

2. Inside this front-end repository, install dependencies:   
`npm install`   

3. Launch front-end on port 3000:    
`npm start`    

4. Front-end is now rendered at URL `http://localhost:3000`.
